import React, {useContext, useEffect, useRef} from 'react';
import ReactDOM from 'react-dom';
import styles from './post.css';
import { CommentAnswerForm} from '../CommentForm';
import { useNavigate, useParams } from 'react-router-dom';
import { usePostsData } from '../../hooks/usePostsData';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../store/rootReducer';
import { IPostData, meRequestAcync } from '../../store/PostsRedux/actionsPostData';
import { TokenState } from '../../store/TokenRedux/tokenReducer';
import { Preview } from '../CardList/Card/Preview';



export function Post() {
  const ref = useRef<HTMLDivElement>(null);
  const navigate = useNavigate();
  let { id } = useParams<string>();
  const data = useSelector<RootState, Array<IPostData>>(state => state.postReducer.data)
  const token = useSelector<RootState, TokenState>(state => state.tokenReducer);
  const dispatch = useDispatch();

  
  if (!id) return null;

 const dataModal = data.filter(post => post.id == id).pop()

 


  useEffect(() => {
    function handleClick(event: MouseEvent) {
      if (event.target instanceof Node && !ref.current?.contains(event.target)) {
        navigate('/posts');
      }
    }
    document.addEventListener('click', handleClick);

    return () => {
      document.removeEventListener('click', handleClick);
    }
  }, []);
  useEffect(() => {
    if (token.token && token.token.length > 0 && token.token !== "undefined") {
        dispatch<any>(meRequestAcync());
      }
     }, [token] )

  const node = document.querySelector('#modal_root');
  if (!node) return null;

  return ReactDOM.createPortal(
    <div className = {styles.modal} ref={ref}>
      <h2 className={styles.title}>{dataModal?.title}</h2>
      <div className = {styles.content}>
        <div className={styles.img}>
        <Preview img={dataModal?.previewImg} />
        </div>
        <p>{dataModal?.title}</p>
      </div>
      {/* <CommentForm /> */}
      <CommentAnswerForm />
    </div>
  , node);
}
