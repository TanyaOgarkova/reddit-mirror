import React from 'react';
import styles from './userInfo.css';
import { MetaData } from '../../CardList/Card/TextContent/MetaData';


export function UserInfo() {
  return (
    <div className = {styles.form}>
        <span className= {styles.createdAt}>
        <span className = {styles.publishedLabel}>Опубликовано </span>
        5 часов назад
        </span>
        <a href="#user-url" className= {styles.username}>Михаил Рогов</a>
        <img className = {styles.avatar} 
        src= 'https://e7.pngegg.com/pngimages/84/165/png-clipart-united-states-avatar-organization-information-user-avatar-service-computer-wallpaper.png'
        alt="avatar" />
    </div>
  );
}
