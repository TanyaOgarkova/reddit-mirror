import React, {useContext, useState, ChangeEvent, MouseEventHandler, useRef, useEffect } from 'react';
import styles from './commentList.css'
import { ICommentContextData } from '../CommentForm';
import { UserInfo } from '../UserInfo';
import { CommentsIcon } from '../../Icon/Icons';
import { commentAnswerContext } from '../../Context/commentAnswerContext';
import { EColors, Text } from '../../Text'



export interface CommentProps {
    comment: ICommentContextData;
    commentKey: string;
}



function CommentBody(comment: CommentProps){
    const [replyOpened, setReplyOpened] = useState(false)
    const {value, onChange, setKey, key} = useContext(commentAnswerContext);
    const textRef = useRef<HTMLTextAreaElement>(null)
  
    function handleAnswerChange(event: ChangeEvent<HTMLTextAreaElement>) {
      onChange(event.target.value)
    }
    function buttonChange(){
        setReplyOpened(prev => !prev)
        setKey('')
        if (!comment.comment.author) return
        if (!replyOpened){
            onChange(comment.comment.author+', ')
        }
        else setKey(comment.commentKey)
    }
    useEffect(
        () => {
            if (!textRef.current) return
            
            textRef.current.focus()
            const val = textRef.current.value
            textRef.current.value = ''
            textRef.current.value = val
        },
        [textRef.current, key]
    )

    return (
        <>
        <UserInfo />
        <div className = {styles.commentText}>
            {comment.comment.text}
        </div> 
        <div className={styles.container}>
                <button onClick={buttonChange} className='commentButton'>
                <div className={styles.line}>   <CommentsIcon />
                    <Text size = {12} color = {EColors.grey99}>  Ответить</Text>
                </div> 
                </button>
                { replyOpened &&  <>
            <textarea ref={textRef} className = {styles.input} value={value} onChange={handleAnswerChange}/>
            </>}
        </div>
        </>
    )
}


function Comment({comment, commentKey}: CommentProps) {
    if (comment.childs){
        return (
            <>
            <CommentBody comment={comment} commentKey={commentKey}/>
            <ul className={styles.commentList}>
                {comment.childs.map((comment, index) => {
                    return <li key={commentKey+'_'+index}>
                    <Comment comment={comment} commentKey={commentKey+'_'+index}/>
                    </li>
                }) }
            </ul>
            </>
        )
    } else{
        return (
            <>
                <CommentBody comment={comment} commentKey={commentKey}/>
            </>
        )
    }

}


export interface CommentListProps {
    comments: ICommentContextData[];
}


export function CommentList(comments: CommentListProps) {
    return (
        <ul className={styles.commentList}>
            {comments.comments.map((comment, index) => {
                return <li key={index}>
                <Comment comment={comment} commentKey={'id_'+index}/>
                </li>
            }) }
        </ul>
    )
}
