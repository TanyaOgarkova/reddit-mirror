import React from 'react';
import { useContext, ChangeEvent } from 'react';
import styles from './comments.css';
import { commentAnswerContext } from '../../Context/commentAnswerContext';
import { FormikErrors, useFormik } from 'formik';
import { create } from 'zustand'


type Store = {
  textValue: string;
  setTextValue: (value: string) => void
}

interface FormValues {
  comments: string;
}

export const textAreaStore = create<Store>()((set) => ({
  textValue: 'Привет из zustand!',
  setTextValue: (value: string) => set((state) => ({ textValue: value })),
}))


export function Comments() {

  const { textValue, setTextValue } = textAreaStore()
  const {setKey} = useContext(commentAnswerContext)


  function handleChange(event: ChangeEvent<HTMLTextAreaElement>) {
    formik.values.comments = event.target.value
    formik.handleChange(event)
    setKey('append')
    setTextValue(event.target.value)
  }
  const validate = (values: FormValues) => {

    const errors: FormikErrors<FormValues> = {};
  
    if (values.comments.length < 3) {
      errors.comments = 'Длинна комментария должна быть больше 3х'
    }
    return errors;
  };
  const formik = useFormik({
    initialValues: {comments: 'asas'},
    validate: validate,
    validateOnChange: true,
    validateOnBlur: true,
    onSubmit: () => {}
  }
)

  return (
  <>
      <textarea 
          name="comment" 
          className = {styles.input} 
          value={textValue}
          onChange={handleChange}
          onBlur={formik.handleBlur}
        />
       <span>{formik.errors.comments}</span>
    <div className={styles.bodyButton}>
    <button type='submit' disabled={!!formik.errors.comments} onSubmit={() => setKey('append')} className = {styles.button}>Комментировать</button>
    </div>
    
  </>
  );
}
