import React, { FormEvent, useState, useContext } from 'react';
import styles from './commentform.css';
import { commentAnswerContext } from '../Context/commentAnswerContext';
import { Comments, textAreaStore } from './Comments';
import { CommentList } from './CommentList/commentList';
import { useSelector } from 'react-redux';
import { RootState } from '../../store/rootReducer';



export interface ICommentContextData {
  id?: string;
  author?: string;
  text?: string;
  rating?: string;
  avatar?: string;
  datePostUtc?: number;
  childs?: ICommentContextData[];
}

export function CommentAnswerForm() {

  const [answerComments, setAnswerComments] = useState<Array<ICommentContextData>>([]);
  const { value, setKey, key } = useContext(commentAnswerContext);
  const { textValue, setTextValue } = textAreaStore()

  function addCommentByKey(key_list: Array<number>, comments: Array<ICommentContextData>, value: string) {
    const key = key_list.pop()
    if (key === undefined) {
      return 
    }
    if (key_list.length > 0 && comments.length > 0){
      const childs = comments[key].childs || []
      if (childs){
        addCommentByKey(key_list=key_list, comments=childs, value=value)
      }
    }
    else {
      const comment: ICommentContextData = {
        text: value,
        author: "Михаил"
      }
      const new_childs = comments[key].childs?.concat(comment)
      comments[key].childs = new_childs? new_childs : [comment]
    }
  }

  function handleAnswerSubmit(event: FormEvent) {
    if (key) {
      if (key == 'append')
      {
        const comment: ICommentContextData = {
          text: textValue,
          author: "Михаил"
        }
        setAnswerComments(answerComments.concat(comment));
        event.preventDefault();
      }
      else{
        const key_list = key.split('_').slice(1).map((el)=> parseInt(el)).reverse()
        addCommentByKey(key_list, answerComments, value)
        setKey('')
      }
    }
    event.preventDefault();
  }

return (
  <form className = {styles.form} onSubmit={handleAnswerSubmit}>
    <Comments />
    <CommentList comments={answerComments}/>
    </form>
);
}
