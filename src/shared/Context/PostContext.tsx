import React, { Children } from "react";
import { usePostsData } from "../../hooks/usePostsData";

export interface IPostContextData {
    id?: string;
    author?: string;
    title?: string;
    rating?: string;
    avatar?: string;
    previewImg?: string;
    datePostUtc?: number;
  }


export const PostContext = React.createContext<Array<IPostContextData>>([]);

export function PostContextProvider({ children}: {children: React.ReactNode}) {
  const [data] = usePostsData()

  return (
    <PostContext.Provider value={data}>
        {children}
    </PostContext.Provider>
  )
}