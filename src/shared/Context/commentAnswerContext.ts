import React from "react";

type CommentContextType = {
    value: string;
    onChange: (value: string) => void;
    
    key: string;
    setKey: (value: string) => void;
    
}

export const commentAnswerContext = React.createContext<CommentContextType>({
    value: '',
    onChange: () => {},
    key: '',
    setKey: () => {}
});
