import * as React from 'react';
import styles from './Icon.css'
import classNames from 'classnames';
import { CommentsIcon, BlockIcon, WarningIcon, SaveIcon, ShareIcon, IconAnon } from './Icons';

export enum EIcon {
    comments,
    share,
    block,
    save,
    warning,
    anon,
}

type ISize = 12 | 14 | 15 | 16;

const Icons = { 
    [EIcon.comments]: <CommentsIcon/>,
    [EIcon.share]: <ShareIcon/>,
    [EIcon.block]: <BlockIcon/>,
    [EIcon.save]: <SaveIcon/>,
    [EIcon.warning]: <WarningIcon/>,
    [EIcon.anon]: <IconAnon/>,
}

interface IIconProps {
    name: EIcon;
    size?: ISize;
}

export function Icon({ name, size}: IIconProps) {
    
    const classes = classNames(
        styles [`s${size}`],
        styles ["icon"],
        )
    return(
       <div  className={classes}>{Icons[name]}</div>
  );
 }
