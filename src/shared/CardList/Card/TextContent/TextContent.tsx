import * as React from 'react';
import styles from './TextContent.css'
import { MetaData} from './MetaData';
import { Title} from './Title';
import { PostProps } from '../Card';

export function TextContent(post: PostProps) {
    return (
<div className={styles.textContent}>
    <MetaData post={post.post}/>
    <Title post={post.post}/>
</div>
      );
    }
    