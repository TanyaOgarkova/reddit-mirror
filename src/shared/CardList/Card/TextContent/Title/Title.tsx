import  React from 'react';
import styles from './Title.css';
import { Link } from 'react-router-dom';
import { PostProps } from '../../Card';

export function Title(post: PostProps) {


    return (
<h2 className = {styles.title}>
    <Link to={`/posts/${post.post.id||1}`} className={styles.postLink}>
        {post.post.title}
      </Link>
</h2>
      );
    }