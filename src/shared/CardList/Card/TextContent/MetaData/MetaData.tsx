import * as React from 'react';
import styles from './MetaData.css'
import { PostProps } from '../../Card';

export function MetaData(post: PostProps) {
    const date_diff  = post.post.datePostUtc ?  new Date(Date.now() - post.post.datePostUtc*1000) : new Date(0)
    return (
<div className={styles.metaData}>
    <div className={styles.userLink}>
        <img className = {styles.avatar} 
        src={post.post.avatar} 
        alt="avatar" />
        <a href="#user-url" className= {styles.username}>{post.post.author}</a>
    </div>
    <span className= {styles.createdAt}>
        <span className = {styles.publishedLabel}>Опубликовано </span>
         {date_diff.getUTCHours()} часов назад
         </span>
</div>
      );
    }
    