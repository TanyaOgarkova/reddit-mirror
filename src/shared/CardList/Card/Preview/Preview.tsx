import * as React from 'react';
import styles from './Preview.css';

interface PreviewProps {
    img?:string;
}

export function Preview(img: PreviewProps) {
    return (
        <div className={styles.preview}>
        <img className = {styles.previewImg} 
         src={img.img} />
     </div>
      );
    }
    