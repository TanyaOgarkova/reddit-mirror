import * as React from 'react';
import styles from './card.css'
import { TextContent } from './TextContent';
import { Preview } from './Preview';
import { Menu } from './Menu';
import { Controls } from './Controls';
import { IPostContextData } from '../../Context/PostContext'; 
import { Break } from '../../Break';


export interface PostProps {
    post: IPostContextData;
}


export function Card(post: PostProps) {

    return (
        <div className= {styles.card}>
            <TextContent post={post.post} />
            <Preview img={post.post.previewImg} />
            <Menu />
            <Controls likes={post.post.rating}/>
            <Break size={16} />
        </div>
      );
}

