import * as React from 'react';
import styles from './Menu.css';'./DropDownMenu.css';
import { MenuIcon } from '../../../Icon/Icons';
import { EColors, Text } from '../../../Text';
import { Coords, DropDown, DropDownAbstract } from './DropDown';
import { useEffect, useRef, useState } from 'react';
import ReactDOM from 'react-dom';

export function Menu() {
  const [isDropDownOpen, setisDropDownOpen] = React.useState(false);

  const controlRef = useRef<HTMLDivElement>(null);
  
  return (
    <div className={styles.menu} ref={controlRef}>
     <DropDownAbstract
      isDropDownOpen = {isDropDownOpen}
      setisDropDownOpen={setisDropDownOpen}
      button  = {
        <button className = {styles.menuButton}>
          <MenuIcon />
        </button>
        }
      >
          <DropDown isDropDownOpen={isDropDownOpen} controlRef={controlRef} />
      </DropDownAbstract>
    </div> )
  }



    