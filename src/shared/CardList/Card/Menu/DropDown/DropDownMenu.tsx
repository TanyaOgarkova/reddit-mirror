import React, { SetStateAction, useEffect, useState } from 'react';
import { EColors, Text } from '../../../../Text';
import { EIcon, Icon } from '../../../../Icon/Icon';
import styles from './dropDownMenu.css';
import classNames from 'classnames';
import ReactDOM from 'react-dom';



interface IDropDownProps {
  button: React.ReactNode;
  children: React.ReactNode;
  isDropDownOpen: boolean;
  setisDropDownOpen: React.Dispatch<SetStateAction<boolean>>;
}

export type Coords = {
    left: number;
    top: number;
};

export function DropDownAbstract({button, children, isDropDownOpen, setisDropDownOpen}: IDropDownProps) {

  return (
      <div className = {styles.container}>
          <div onClick={() => setisDropDownOpen(!isDropDownOpen)}>
              {button}
          </div>
          {isDropDownOpen && (
              <div className = {styles.listContainer}>
                  <div className = {styles.list} onClick={() => setisDropDownOpen(false)}>
                      {children}
                  </div>
              </div>
          )}
      </div>
  )
}

interface dropDownProps{
  controlRef: React.RefObject<HTMLDivElement>,
  isDropDownOpen: boolean,
}

export function DropDown({controlRef, isDropDownOpen}: dropDownProps) {
    const node = document.querySelector('#dropdown_root');
    if (!node) return (<></>)

    const [coords, setCoords] = useState<Coords>({top:0, left: 0});

    const getCoords = (): Coords => {
      const box = controlRef.current?.getBoundingClientRect();

      if (box) {
        return {
          left: box.left+box.width,
          top: box.top+window.scrollY,
        };
      }
      return {
        left: 0,
        top: 0,
      };
    };

    useEffect(() => {setCoords(getCoords())}, [controlRef, isDropDownOpen])

    
    return ReactDOM.createPortal((
      <div className={styles.dropdown} style={{top: coords.top, left: coords.left}}>

        <ul className={styles.menuItemsList}>
               <li className={classNames(styles.menuItem, styles.visible)}>
                <Icon name={EIcon.comments}/>
                <Text size = {12} color = {EColors.grey99}>Комментарии</Text>
                </li>

                <div className = {classNames(styles.divider, styles.visible)} />

                <li className={classNames(styles.menuItem, styles.visible)}>
                <Icon name={EIcon.share}/>
                <Text size = {12} color = {EColors.grey99}>Поделиться</Text>
                </li>

                <div className = {classNames(styles.divider, styles.visible)} />

               <li className = {styles.menuItem}>
               <Icon name={EIcon.block}/>
                 <Text size = {12} color = {EColors.grey99}>Скрыть</Text>
                 </li>

                 <div className = {classNames(styles.divider, styles.visible)} />

                 <li className={classNames(styles.menuItem, styles.visible)}>
                 <Icon name={EIcon.save}/>
                <Text size = {12} color = {EColors.grey99}>Сохранить</Text>
                </li>

                 <div className = {styles.divider} />

                 <li className = {styles.menuItem}>
                 <Icon name={EIcon.warning}/>
                <Text size = {12} color = {EColors.grey99}>Пожаловаться </Text>
                </li>
        </ul>
                  <button className={styles.closeButton}>
                  <Text mobileSize = {12} size = {14} color = {EColors.grey66}>
                    Закрыть
                  </Text>
                </button>
        </div>

      ), node);
    }