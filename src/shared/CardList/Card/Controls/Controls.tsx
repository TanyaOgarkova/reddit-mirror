import * as React from 'react';
import styles from './Controls.css'
import { CommentsButton } from './CommentsButton';
import { Actions } from './Actions';
import { KarmaCounter } from './KarmaCounter';
import { Likes } from './KarmaCounter';

export function Controls(likes: Likes) {
    return (
        <div className={styles.controls}>
        <KarmaCounter likes={likes.likes} />
        <CommentsButton />
        <Actions />
    </div>
      );
    }
    