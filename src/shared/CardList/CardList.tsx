import React, {useContext, useEffect, useRef, useState} from 'react';
import styles from './cardslist.css'
import { Card } from './Card';
import { PostContext } from '../Context/PostContext';
import axios from 'axios';
import { useSelector } from 'react-redux';
import { RootState } from '../../store/rootReducer';

 export function CardList() {
  const token = useSelector<RootState>(state => state.tokenReducer.token);
  const posts = useContext(PostContext);
  const [loading, setLoading] = useState(false);
  const [nextAfter, setNextAfter] = useState('');
  const [loadAmount, setLoadAmount] = useState<number>(0);

  const bottomOfList = useRef<HTMLDivElement>(null);

  useEffect(() => {
    async function load() {
      if (loading) return
      setLoading(true);

      try {
        const { data: {data: {after, children}} } = await axios.get('https://oauth.reddit.com/best?sr_detail=true', {
          headers: { Authorization: `Bearer ${token}` },
          params: {
            limit: 10,
            after: nextAfter,
          }
        });
        setNextAfter(after);
        const userData = children.map(
          (item: { data: any }) => ({
            id: item.data.id,
            author: item.data.author,
            title: item.data.title,
            rating: item.data.rating,
            avatar: item.data.sr_detail.icon_img
            ? item.data.sr_detail.icon_img 
            : 'https://cdn-icons-png.flaticon.com/512/5639/5639854.png',
            previewImg: item.data.preview
            ? item.data.preview.images?.[0].source.url.replace(
              /(\&amp\;)/g,
              "&"
            )
            :'https://cdn-icons-png.flaticon.com/512/5639/5639854.png',
            datePostUtc: item.data.created_utc,
          })
        );
        posts.push(...userData);
        } catch (error) {
          console.log(String(error));
          }
      setLoading(false);
      setLoadAmount(loadAmount + 1);
    }


    const observer = new IntersectionObserver((entries) => {
      if (entries[0].isIntersecting && loadAmount < 3 && token) {
        load();
      }
    }, {
      rootMargin: '10px',
    });
    if ( bottomOfList.current) {
      observer.observe(bottomOfList.current);
    }
    return () => {
    if ( bottomOfList.current) {
      observer.unobserve(bottomOfList.current);
    }
   }
  }, [bottomOfList.current, nextAfter,token, loadAmount]);

    return ( <>
        <ul className= {styles.CardList}>
          {posts.map((post, index) => {
            return <li key={'id_'+index.toString()}>
              <Card post={post} />
              </li>
          }) }
          <div ref={ bottomOfList }/>
        </ul>
        {loading && (
        <div className={styles.load}>Загрузка...</div>
        )}
        {loadAmount >= 3 &&  (
          <button
            className={styles.buttonMore}
            onClick={() => setLoadAmount(0)}
          >
            Показать еще
          </button>
        )}
            </>
      );
}





