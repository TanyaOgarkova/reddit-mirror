import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { saveToken } from '../store/TokenRedux/actionsTokenData';
import { TokenState } from '../store/TokenRedux/tokenReducer';
import { ThunkAction } from 'redux-thunk';


export function useToken() {
    const token = useSelector<TokenState, string>(
      (state) => state.token
    );
    const dispatch = useDispatch();
  
    useEffect(() => {
      dispatch<any>(saveToken());
    }, []);
  
    return [token];
  }