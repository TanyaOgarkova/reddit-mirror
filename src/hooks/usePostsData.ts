import  { useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from "../store/rootReducer";
import { IPostData, meRequestAcync } from "../store/PostsRedux/actionsPostData";
import { TokenState } from "../store/TokenRedux/tokenReducer";



export function usePostsData(){
    const data = useSelector<RootState, Array<IPostData>>(state => state.postReducer.data)
    const token = useSelector<RootState, TokenState>(state => state.tokenReducer);
    const dispatch = useDispatch();

    useEffect(() => {
      if (token.token && token.token.length > 0 && token.token !== "undefined") {
          dispatch<any>(meRequestAcync());
        }
       }, [token] )

    return [data]
} 
