import './main.global.css'
import React, { useEffect, useState } from 'react';
import { hot } from 'react-hot-loader/root';
import { CardList } from './shared/CardList/CardList';
import { Layout } from './shared/Layout/Layout';
import { Header } from './shared/Header';
import { Content } from './shared/Content';

import { commentAnswerContext } from './shared/Context/commentAnswerContext';

import { legacy_createStore as createStore, applyMiddleware, Action} from 'redux';
import thunk from 'redux-thunk'
import { Provider, useDispatch } from 'react-redux';
import { composeWithDevTools } from '@redux-devtools/extension';
import { rootReducer} from './store/rootReducer';
import { useToken } from './hooks/useToken';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { Post } from './shared/Post';
import { NotFoundPage } from './NotFoundPage';



const store = createStore(rootReducer, composeWithDevTools(
    applyMiddleware(thunk),
));

function AppComponent() {
    const dispatch = useDispatch();

    const [token] = useToken();
  

    const [commentAnswerValue, setCommentAnswerValue] = useState('');
    const [commentKeyValue, setCommentKeyValue] = useState('');
    const [mounted, setMounted] = useState<null | true>(null);

    const CommentAnswerProvider = commentAnswerContext.Provider;

    useEffect(() => {
        setMounted(true);
      }, []);
    

    return (
        mounted && (
            <BrowserRouter>
                 <CommentAnswerProvider value={{
                        value: commentAnswerValue,
                        onChange: setCommentAnswerValue,
                        key: commentKeyValue,
                        setKey: setCommentKeyValue,
                    }}>
                                <Layout>
                                    <Header />
                                    <Content>
                                        <Routes>
                                            <Route path={`/`} element={<Navigate to="/posts" replace />} />
                                            <Route path={`/posts`} element={<CardList />}/>
                                            <Route path='/posts/:id' element={<Post />} />
                                            <Route
                                                path={`/auth`}
                                                element={<Navigate to="/posts" replace />}
                                            />
                                            <Route path="*" element={<Navigate to="/404" replace />} />
                                            <Route path="/404" element={<NotFoundPage />} />
                                        </Routes>
                                    </Content>
                                </Layout>
                </CommentAnswerProvider>
            </BrowserRouter>
    )
     );
}

export const App = hot(() => 
<Provider store={store}>
    <AppComponent />
</Provider>
);
