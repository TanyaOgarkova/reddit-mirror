import { configureStore } from '@reduxjs/toolkit'
import { useDispatch } from 'react-redux'
import { commentReducer } from './UpdateCommentRedux/updateCommentReducer';
import { tokenReducer } from './TokenRedux/tokenReducer';
import { postReducer } from './PostsRedux/postReducer';

export const store = configureStore({
  reducer: {
    tokenReducer,
    postReducer,
    commentReducer,
  },
  devTools: true,
});

export const useStoreDispatch = () => useDispatch<typeof store.dispatch>()
export type RootState = ReturnType<typeof store.getState>