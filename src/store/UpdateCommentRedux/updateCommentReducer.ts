import { ActionCreator, Reducer } from "redux";


const UPDATE_COMMENT = 'UPDATE_COMMENT';

type UpdateCommentAction = {
    type: typeof UPDATE_COMMENT;
    text: string;
}

export const updateComment: ActionCreator<UpdateCommentAction> = (text: string) => ({
    type: UPDATE_COMMENT,
    text,
});

export type CommentState = {
    commentText: string;
    token: string;
}

const initialState = {
    commentText: 'Привет из Zustand!',
    token: 'undefined',
};

export const commentReducer: Reducer<CommentState> = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_COMMENT:{
         return {
            ...state,
            commentText: action.text,
         };
        }
         default:
            {
            return state;
            }
   }
 };
 