import { Action, ActionCreator } from "redux";
import { ThunkAction } from "redux-thunk";
import { RootState } from "../rootReducer";
import axios from "axios";


export const ME_TOKEN = 'ME_TOKEN';

export function meToken(token: string) {
    return {
      type: ME_TOKEN,
      token,
    };
  }

interface myRequest{
    query:{
        code: ''
    }
}
interface myResponce{
    send: (body: string) => {}
}

type ActionThunk = ThunkAction<void, RootState, unknown, Action<string>>;

export const saveToken = (): ActionThunk => (dispatch, getState) => {
    const queryParams = new URLSearchParams(window.location.search);
    const code = queryParams.get('code');
    if (!code) return;
    axios.post (
        'https://www.reddit.com/api/v1/access_token',
        `grant_type=authorization_code&code=${code}&redirect_uri=http://localhost:3000/auth`,
        {
          auth: { username: "rTX_tuVOv-vGErzcSbZb2A", password: 'SvB3aUy-yJYIZ826HP7uAIJgRHVGOg'},
          headers: { 'Content-type': 'application/x-www-form-urlencoded' }
        }
      )
      .then(({ data }) => {
        if (!data['access_token']) return;
        dispatch(meToken(data['access_token']));
      })
      .catch((error) => 
      {
          console.log(error);
      }
      );
}