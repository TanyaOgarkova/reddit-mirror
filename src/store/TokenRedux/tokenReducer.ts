import { Reducer } from "redux";
import { ME_TOKEN } from "./actionsTokenData";



export type TokenState = {
    token: string,
}

const initialState: TokenState = {
    token: '',
};

export const tokenReducer: Reducer<TokenState> = (state = initialState, action) => {
    switch (action.type) {
        case ME_TOKEN:
           return {
            ...state,
            token: action.token,
           };
           default:
            return state;
    }
}