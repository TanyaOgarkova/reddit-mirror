import { Action, ActionCreator } from "redux";
import { ThunkAction } from "redux-thunk";
import { RootState } from "../rootReducer";
import axios from "axios";
import { useState } from "react";

export const ME_REQUEST = 'ME_REQUEST';

export type MeRequestAction ={
    type: typeof ME_REQUEST;
}
export const meRequest: ActionCreator<MeRequestAction> = () => ({
    type: ME_REQUEST,
});

 export interface IPostData {
    id?: string;
    author?: string;
    title?: string;
    rating?: string;
    avatar?: string;
    previewImg?: string;
  }

export const ME_REQUEST_SUCCESS = 'ME_REQUEST_SUCCESS';

export type MeRequestSuccessAction ={
    type: typeof ME_REQUEST_SUCCESS;
    data: Array<IPostData>;
}
export const meRequestSuccess: ActionCreator<MeRequestSuccessAction> = (data: Array<IPostData>) => ({
    type: ME_REQUEST_SUCCESS,
    data,
});

export const ME_REQUEST_ERROR = 'ME_REQUEST_ERROR';

export type MeRequestErrorAction ={
    type: typeof ME_REQUEST_ERROR;
    error: string;
}
export const meRequestError: ActionCreator<MeRequestErrorAction> = (error: string) => ({
    type: ME_REQUEST_ERROR,
    error,
});

type ActionThunk = ThunkAction<void, RootState, unknown, Action<string>>;

export const meRequestAcync = (): ActionThunk => (dispatch, getState) => {

    dispatch(meRequest());
    axios.get('https://oauth.reddit.com/best?sr_detail=true', {
      headers: { Authorization: `Bearer ${getState().tokenReducer.token}` },
      params: {
        limit: 10,
      }
    })
      .then((resp) => {
        const userData = resp.data.data.children.map(
          (item: { data: any }) => ({
            id: item.data.id,
            author: item.data.author,
            title: item.data.title,
            rating: item.data.rating,
            avatar: item.data.sr_detail.icon_img
            ? item.data.sr_detail.icon_img 
            : 'https://cdn-icons-png.flaticon.com/512/5639/5639854.png',
            previewImg: item.data.preview
            ? item.data.preview.images?.[0].source.url.replace(
              /(\&amp\;)/g,
              "&"
            )
            :'https://cdn-icons-png.flaticon.com/512/5639/5639854.png',
            datePostUtc: item.data.created_utc,
          })
        );
        dispatch(meRequestSuccess(userData));
      })
      .catch((error) => {
        console.log(error);
        dispatch(meRequestError(String(error)));
      });
}