import { Reducer } from "redux";
import { IPostData, ME_REQUEST, ME_REQUEST_ERROR, ME_REQUEST_SUCCESS, MeRequestAction, MeRequestErrorAction, MeRequestSuccessAction } from "./actionsPostData";

export type MeState = {
    error: string;
    data: Array<IPostData>;
}



const initialState: MeState = {
    error: '',
    data: []
};

type MeActions =  MeRequestAction | MeRequestSuccessAction | MeRequestErrorAction;

export const postReducer: Reducer<MeState, MeActions> = (state = initialState, action) => {
    switch (action.type) {
        case ME_REQUEST:
           return {
            ...state,
           };
           case ME_REQUEST_SUCCESS:
            return {
             ...state,
             data: action.data,
            };
           case ME_REQUEST_ERROR:
            return {
             ...state,
             error: action.error,
            };
           default:
            return state;
    }
}