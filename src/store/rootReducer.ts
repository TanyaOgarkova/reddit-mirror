import {  Reducer, combineReducers } from "redux";
import { MeState,postReducer } from "./PostsRedux/postReducer";
import { TokenState, tokenReducer } from "./TokenRedux/tokenReducer";
import { CommentState, commentReducer } from "./UpdateCommentRedux/updateCommentReducer";

export type RootState = {
    tokenReducer: TokenState;
    postReducer: MeState;
    commentReducer: CommentState;
}

export const rootReducer: Reducer<RootState> = combineReducers({
    tokenReducer,
    postReducer,
    commentReducer,
  });

